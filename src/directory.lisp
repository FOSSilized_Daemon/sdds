(in-package :sdds)

;; The directory.lisp file contains all of the Common Lisp
;; related to creating files and directories.
;; When this file is loaded the below Common Lisp will
;; do the following.
;;    1. Expose functions for creating user files and directories.

(defun create-user-directory (directory-list &key (verbose nil) (non-existant nil))
  "(create-user-directory ()) creates all user directories within directory-list."
  (when (fboundp 'pre-directory-hook)
    (funcall 'pre-directory-hook))

  (dolist (current-directory directory-list)
    (if non-existant
      (progn
        (when (not (uiop:file-exists-p current-directory))
          (ensure-directories-exist current-directory :verbose verbose)))

      (dolist (current-directory directory-list)
        (ensure-directories-exist current-directory :verbose verbose))))

  (when (fboundp 'post-directory-hook)
    (funcall 'post-directory-hook)))

(defun update-user-directory (directory-list &key (verbose nil))
  "(update-user-directory ()) creates all *user-directories* that do not exist."
  (create-user-directory directory-list :verbose verbose :non-existant t))

(defun delete-user-directory (directory-list)
  "(delete-user-directory ()) deletes all *user-directories*."
  (dolist (current-directory directory-list)
    (uiop:delete-directory-tree (pathname current-directory) :if-does-not-exist :ignore)))

(defun check-user-directory (directory-list)
  "(check-user-directory ()) checks for non-existant directories within directory-list."
  (dolist (current-directory directory-list)
    (when (not (uiop:file-exists-p current-directory))
      (os:log-warn (format nil "The directory ~s does not exist." current-directory)))))

(defun user-directoy-permission (directory-list directory-permission)
  "(user-directory-permission ()) sets directory-list to have directory-permission."
  (when (fboundp 'pre-directory-permission-hook)
    (funcall 'pre-directory-permission-hook))

  (dolist (current-directory directory-list)
    (uiop:run-program (list "chmod" directory-permission))))
